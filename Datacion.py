from math import log

cantidadMuestras = int(input("Cuantas muestras va a ingresar: "))
listaMuestras = list()

for i in range(cantidadMuestras):
    masa = float(input("Ingrese la masa de la roca: "))
    k = float(input("Ingrese la cantidad de potasio-40: "))
    ar = float(input("Ingrese la cantidad de argon-40: "))
    edad = (1.248 * (10 ** 9) / log(2)) * log((k + (ar / 0.109)) / k)
    era = "Pre-Paleozoica"
    if edad < 65500000:
        era = "Cenozoica"
    elif edad < 251000000:
        era = "Mesozoica"
    elif edad < 542000000:
        era = "Paleozoica"
    muestra = {
        "IdMuestra" : i+1,
        "Masa" : masa,
        "Potasio-40" : k,
        "Argon-40" : ar,
        "Edad" : edad,
        "Era" : era
    }
    listaMuestras.append(muestra)
    print()
    print("**** Muestra " + str(i+1) + " ingresada ****")
    print()

print("Se han ingresado " + str(len(listaMuestras)) + " muestras en el sistema.")
print()

print("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
for muestra in listaMuestras:
    print()
    print("La muestra " + str(muestra["IdMuestra"]) + " tiene la siguente informacion:")
    print("Id: " + str(muestra["IdMuestra"]))
    print("Masa: " + str(muestra["Masa"]))
    print("Potasio-40: " + str(muestra["Potasio-40"]))
    print("Argon-40: " + str(muestra["Argon-40"]))
    print("Edad: " + str(muestra["Edad"]))
    print("Era: " + str(muestra["Era"]))
    print("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")

print("Terminado")
